## Release Summary

This release adds a few new functions and fixes a few bugs under macOS.

## Test Environments

* Local macOS Mohave, R-release
* Local Windows 10, R-devel
* R-hub (various platforms)
* Semaphore CI + Ubuntu 14.04, R-devel and R-release
* AppVeyor + Windows Server 2012, R-devel

## R CMD check results

There were no ERRORs or WARNINGs.

## Downstream dependencies

A new version of the downstream assertive package will be uploaded once this 
package is successfully on CRAN.
