msgid ""
msgstr ""
"Project-Id-Version: assertive.reflection 0.0-4\n"
"Report-Msgid-Bugs-To: https://bitbucket.org/richierocks/assertive.reflection/issues\n"
"POT-Creation-Date: 2017-06-18 19:02\n"
"PO-Revision-Date: 2017-06-25 21:36\n"
"Last-Translator: Richard Cotton <richierocks@gmail.com>\n"
"Language-Team:  \n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

msgid "Some packages in %s are out of date."
msgstr "% s의 일부 패키지가 오래되었습니다."

msgid "%s are not all on the operating system path."
msgstr "% s이 (가) 운영 체제 경로에 모두있는 것은 아닙니다."

msgid "%s are all not on the operating system path."
msgstr "% s은 (는) 모두 운영 체제 경로에 없습니다."

msgid "R is %d bit."
msgstr "R은 % d 비트입니다."

msgid "This version of R is %s but the current version is %s."
msgstr "이 R 버전은 % s이지만 현재 버전은 % s입니다."

msgid "R has been compiled without support for locales."
msgstr "R은 로케일 지원없이 컴파일되었습니다."

msgid "The locale convention for a (%s) decimal point has not been defined."
msgstr "(% s) 소수점에 대한 로케일 규칙이 정의되지 않았습니다."

msgid "The locale convention is to use a '%s' for a (%s) decimal point."
msgstr "로케일 규칙은 (% s) 소수점으로 '% s'을 (를) 사용합니다."

msgid "You are running the server version of RStudio."
msgstr "RStudio의 서버 버전을 실행하고 있습니다."

msgid "You are running the desktop version of RStudio."
msgstr "RStudio의 데스크톱 버전을 실행하고 있습니다."

msgid "'tools:rstudio' is not loaded, so the RStudio API is not available."
msgstr "'tools : rstudio'가로드되지 않으므로 RStudio API를 사용할 수 없습니다."

msgid "You are using an old version of RStudio, which does not tell you version information."
msgstr "버전 정보를 알려주지 않는 구 버전의 RStudio를 사용하고 있습니다."

msgid "The operating system is not %s. R reports it as: Sys.info()['sysname'] = %s, .Platform$OS = %s."
msgstr "운영 체제가 % s이 (가) 아닙니다. R은 다음과 같이보고합니다. Sys.info () [ 'sysname'] = % s, .Platform $ OS = % s."

msgid "The operating system is not Windows %s. R reports it as: Windows %s."
msgstr "운영 체제가 Windows % s이 (가) 아닙니다. R은 다음과 같이보고합니다 : Windows % s."

msgid "The operating system is not %s %s. R reports it as: %s %s."
msgstr "운영 체제가 % s % s이 (가) 아닙니다. R은 그것을 % s % s (으)로보고합니다."

msgid "R's build type is %s, not %s."
msgstr "R의 빌드 유형은 % s가 아니라 % s입니다."

msgid "R does not have %s capability."
msgstr "R에는 % s 기능이 없습니다."

msgid "%s capability is not declared for versions of R before %s."
msgstr "% s 기능은 % s 이전의 R 버전에 대해 선언되지 않았습니다."

msgid "R cannot find the %s tool."
msgid_plural "R cannot find the %s tools."
msgstr[0] "R이 % s 도구를 찾을 수 없습니다."

